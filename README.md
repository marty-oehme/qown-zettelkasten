# QOwn-Zettelkasten

Simple Zettelkasten functionality for QOwnNotes.

Note: If using standard markdown links for Zettel Anchors - the autocomplete function of QOwnNotes will first attempt to find a note with the *name* of the link. By default it will then ask you if you want to create that note. If you want to make use of the autocomplete for Zettelkasten links primarily (and intend to use standard markdown links) make sure to tick "Don't as again" before clicking "No". Subseqent autocompletes will instantly jump to the selected Zettel.